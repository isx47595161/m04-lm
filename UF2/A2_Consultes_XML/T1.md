Consultar documents XML
=======================

MP4UF2A2T1

Introducció al mòdul `ElementTree` de Python

Tècniques convenients de Python
-------------------------------

Et caldrà recordar moltes de les tècniques de Python pel tractament de
llistes i diccionaris que ja has estudiat. A més a més, presentem a
continuació altres tècniques útils que potser no coneixes encara:

-   **Guia d’estil per Python**: si et vols estalviar estudiar el
    [PEP8](https://www.python.org/dev/peps/pep-0008/) per aplicar
    l’estil recomanat en escriure codi de Python tens una solució
    perfecte en el [Ninja-IDE](http://www.ninja-ide.org/). Tan sols et
    cal executar `yum install ninja-ide -y` i sempre escriuràs Python
    com els professionals.
-   **Comprensió de llistes**: les comprensions de llistes ofereixen una
    sintaxi compacta, inspirada en la notació matemàtica de conjunts,
    per expressar llistes de forma declarativa, sense assignacions
    o bucles. Un senzills exemples faran fàcil entendre les seves
    possibilitats:\
    \
    -   Els quadrats dels 10 primers enters:
        `[ i ** 2 for i in range(10) ]`
    -   Els 10 primers enters i els seus quadrats:
        `[ (i, i ** 2) for i in range(10) ]`
    -   Nombres parells menors de 40:
        `[ i for i in range(1, 40) if i%2 == 0]`
    -   Múltiples de 7 menors de 100:
        `[ i for i in range(7, 100) if i%7 == 0]`
    -   Paraules en majúscules:
        `[s.upper() for s in 'The quick brown fox jumps over the lazy dog'.split()]`
    -   Treure un nivell de llistes:
        `[x for xs in [[1,2,3], [4,5,6], [7,8,9]] for x in xs]`

    \
    La relació entre les comprensions de llistes i el seu equivalent
    procedural és aquesta:\
    \
      ----------------------------------- ----- ---------------------
      ys = \[f(x) for x in xs\]            ⇔    ys = \[\]\
                                                for x in xs:\
                                                  ys.append(f(x))

      ys = \[x for x in xs if g(x)\]       ⇔    ys = \[\]\
                                                for x in xs:\
                                                  if g(x):\
                                                    ys.append(x)

      ys = \[f(x) for x in xs if g(x)\]    ⇔    ys = \[\]\
                                                for x in xs:\
                                                  if g(x):\
                                                    ys.append(f(x))
      ----------------------------------- ----- ---------------------

    \
-   **Programació funcional**: Python sempre a proporcionat diverses
    funcions típiques de la programació funcional, com ara `map` i
    `filter` que tenen aquesta correspondència amb les comprensions de
    llistes:\
    \
      ------------------------------ ----- -----------------------
      \[f(x) for x in xs\]            ⇔    map(f, xs)
      \[x for x in xs if g(x)\]       ⇔    filter(g, xs)
      \[f(x) for x in xs if g(x)\]    ⇔    map(f, filter(g, xs))
      ------------------------------ ----- -----------------------

    \
-   **Calcul de totals**: les funcions `sum`, `max` i `min` s'apliquen a
    qualsevol iterable (llistes, tuples, fitxers, etc.).
-   **Expressió condicional**: l'expressió de Python `a if b else c`
    proporciona el valor `a` si l'expressió `b` és certa, i el valor `b`
    en cas contrari. El valor que no es retorna no és avaluat.
    L'expressió és similar a `b and a or c`, però aquesta darrera falla
    en alguns casos obscurs que ara no comentarem.

El mòdul `ElementTree`
----------------------

El mòdul de Python `xml.etree.ElementTree` proporciona diverses
facilitats per la creació i manipulació d’arbres d’elements. Ara tan
sols utilitzarem unes poques prestacions d'aquest mòdul, presentades en
forma d'exemples més endavant. Els conceptes principals a considerar són
aquests:

-   La funció `parse` retorna un objecte de tipus `ElementTree`.
-   El mètode `getroot` dels objectes de tipus `ElementTree` retorna un
    objecte de tipus `Element`.
-   Les propietats principals dels elements són `tag`, `text` i
    `attrib`.
-   Els elements comparteixen característiques de les llistes (pels
    seus subelements) i dels diccionaris (pels seus atributs).
-   Els mètodes dels elements que farem servir per ara són `findall` i
    `find`.

El llenguatge XPath
-------------------
- 	[XPath](https://www.josedomingo.org/pledin/2015/01/trabajar-con-ficheros-xml-desde-python_1/)
-	[XPath a w3chools](https://www.w3schools.com/xml/xpath_syntax.asp)
-	[Python XML processing with lxml](http://infohost.nmt.edu/tcc/help/pubs/pylxml/web/index.html)
-	[Multitud l'exemples amb XPath](http://www.mclibre.org/consultar/xml/lecciones/xml-xpath.html)

En cridar a alguns mètodes dels objectes de tipus `Element` podem fer
servir un petit subconjunt del llenguatge XPath:

-   `Nom`: el nom (etiqueta) de qualsevol element fill de
    l’element actual.
-   `Nom1/Nom2/...`: ruta relativa amb noms d’elements.
-   `*`: equival a qualsevol nom d’element.
-   `.//Nom`: element, descendent de l’element actual, en qualsevol lloc
    de l’arbre d’elements.
-   `//*`: tots els elements de l’arbre d’elements.


Pràctiques
----------

 [exercicis.md](aux/exercicis.md) amb el següent [XML](aux/poblacions.xml)

Més enllaços recomanats
-------------------

-   [Python 2.5 Reference
    Card](http://www.pa.msu.edu/~duxbury/courses/phy480/python_refcard.pdf)
    (imprimeix en paper aquest document, i conserva una versió
    [local](aux/python-2-5.pdf))
-   [Python Quick Reference Card](http://rgruet.free.fr/) (versió
    [local](aux/python-QR.pdf))
-   Copia local de la [Python v2.7
    documentation](file:///usr/share/doc/python-docs/html/index.html)
    (paquet RPM `python-docs`).
-   Mòdul `xml.etree.ElementTree` en The Python Standard Library

Exemples de consultes I
-----------------------

Les expressions de Python presentades a continuació assumeixen aquest
entorn de treball:

    import xml.etree.ElementTree as ET
    tree = ET.parse("cd_catalog.xml")
    root = tree.getroot()

1.  Element arrel: `tree.getroot()`
2.  Nom de l’element arrel: `root.tag`
3.  El primer CD: `ET.dump(root[0])`
4.  El darrer CD: `ET.dump(root[-1])`
5.  Quantitat de CDs: `len(root)`
6.  Tots el CDs: `list(root)`
7.  Visualitzar tots el CDs: `ET.dump(root)`
8.  Quantitat total d’elements: `len(root.findall(".//*"))`
9.  Els CDs en ordre invers: `list(reversed(root))`

Exemples de consultes II
------------------------

Les expressions de Python presentades a continuació assumeixen aquest
entorn de treball:

    import xml.etree.ElementTree as ET
    tree = ET.parse("cd_catalog.xml")
    root = tree.getroot()

1.  Tots els noms d’artistes:
    `[e.text for e in root.findall("CD/ARTIST")]`; alternativa:
    `[e.text for e in root.findall(".//ARTIST")]`
2.  CDs d’abans de 1980:
    `[e for e in root if int(e.find("YEAR").text) < 1980]`
3.  CDs del Regne Unit:
    `[e for e in root if e.find("COUNTRY").text == "UK"]`
4.  Nom dels artistes del Regne Unit:
    `[e.find("ARTIST").text for e in root if e.find("COUNTRY").text == "UK"]`
5.  Noms curts de CDs:
    `[e.text for e in root.findall("CD/TITLE") if len(e.text) < 5]`
6.  CDs sense preu: `[e for e in root if e.find("PRICE") is None]`
7.  CDs amb atributs: `[e for e in root if len(e.attrib) > 0]`
8.  Suma dels preus de tots els CDs:
    `sum([float(e.text) for e in root.findall("CD/PRICE")])`
9.  El preu del CD més car:
    `max([float(e.text) for e in root.findall("CD/PRICE")])`
10. L’any del CD més antic:
    `min([int(e.text) for e in root.findall("CD/YEAR")])`

Pràctiques
----------

-   Reescriu alguns dels exemples de comprensions de llistes fent servir
    bucles tradicionals.
-   Reescriu alguns dels exemples de comprensions de llistes fent servir
    les funcions `map` i `filter`.
-   Executa les consultes de les seccions Exemples de consultes.
-   Consulta en el mòdul `xml.etree.ElementTree` els mètodes usats en
    els exemples.
-   Contesta aquestes [preguntes](aux/ejercicios_xpath_preguntas.txt)
    (afegint al fitxer de les preguntes les respostes) en base a aquest
    [document](aux/ejercicios_xpath_documento.xml).

