Verificar documents ben formats
===============================

MP4UF1A1T3

Us d’eines per verificar documents XML

Eines de verificació
--------------------

-   `xmlwf`(1): llibreria expat (llibreria molt usada, com per exemple
    en el projecte Mozilla).
-   `xmllint`(1): llibreria libxml2 (llibreria fonamental del projecte
    Gnome i altres projectes importants).

Enllaços recomanats
-------------------

-   [W3schools: XML Tutorial](http://www.w3schools.com/xml/)
-   [W3schools: CD Catalog](http://www.w3schools.com/xml/cd_catalog.xml)
-   [/etc/fonts/fonts.conf](file:///etc/fonts/fonts.conf)

Pràctiques
----------

-   Amb una còpia del [catàleg de CDs](aux/cd_catalog.xml) o del fitxer
    [/etc/fonts/fonts.conf](file:///etc/fonts/fonts.conf) (en la còpia
    pots eliminar la declaració de tipus de document) provoca
    deliberadament tots els errors estudiats (verifica els documents amb
    `xmlwf` o `xmllint --noout`).
-   Identifica les parts de l’XML Syntax Quick Reference que tracten
    dels documents ben formats.
-   Practica els tutorials citats en la llista d’enllaços: per exemple,
    crea localment tots els documents presentats, verifica el seu
    contingut, etc.
-   Usa l’ordre `xmllint`(1) (opció `--format`) per formatar
    correctament un document XML (previament
    des-formatat deliberadament).

