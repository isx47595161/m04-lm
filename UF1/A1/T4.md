Definir i incloure entitats
===========================

MP4UF1A1T4

Treball amb entitats generals internes i externes

Entitats de caràcter (o numèriques)
-----------------------------------

-   Les entitats de caràcter permeten inserir en el document el codi
    numèric de qualsevol caràcter Unicode, com ara l’euro: &\#8364; (€).
-   El codi del caràcter es pot escriure en base decimal o hexadecimal.

Entitats generals internes i externes
-------------------------------------

Les entitats generals proporcionen un simple mecanisme de substitució de
text a utilitzar dins dels documents XML.

-   La declaració de tipus de document (`<!DOCTYPE…`) conté les
    declaracions d’entitat.
-   Les entitats generals internes es **defineixen** (`<ENTITY…>`) dins
    de les claus `[…]` del DTD o en un fitxer separat (cosa que de
    moment no farem), i s’utilitzen (es **referèncien** ) en el document
    XML (no dins del DTD, que encara no hem estudiat).
-   Les entitats generals externes es defineixen com les internes però
    afegint la paraula reservada `SYSTEM`, i permeten incloure un
    fragment de document (escrit en un fitxer separat) dins d’un altre
    document XML.
-   Les entitats generals (ja siguin internes o externes) es referèncien
    dins del document amb la sintaxi `&NOM;`.
-   En aquesta activitat encara no treballarem amb les entitats
    de paràmetre.

L’ordre `xmllint`(1) por ser usada per expandir les entitats generals
d’un document XML.

Enllaços recomanats
-------------------

-   [DTD Tutorial](http://www.w3schools.com/xml/xml_dtd.asp)
-   [W3schools: CD Catalog](http://www.w3schools.com/xml/cd_catalog.xml)

Pràctiques
----------

-   Prenent com a base el catàleg de CDs del tutorial d’XML, prepara un
    document XML que utilitzi entitats generals internes i externes
    (sempre declarades en el subconjunt intern del DTD).
-   En el document declara entitats generals internes per 5 caràcters
    que no apareguin en el teclat, com ara el €.
-   En el fitxer associat a una entitat general externa posa, per
    exemple, el text de la descripció d’un CD.
-   Expandeix el document fent servir l’ordre `xmllint`(1) (opcions:
    `--noent --encode utf8 --loaddtd`).

